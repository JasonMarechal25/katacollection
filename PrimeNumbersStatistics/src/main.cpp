#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include <map>

int lastDigit(int n) {
	return n % 10;
}

struct data {
	std::map<int, int> count = {
			{1, 0},
			{3, 0},
			{7, 0},
			{9, 0},
	};
	int total = 0;
};

std::map<int, data> countFollowingLastDigit{ {1, data{}}, {3, data{}}, {7, data{}}, {9, data{}} };

int main() {
	std::cout << "Hello, World! " << std::endl;
	std::cout << "From/to\t|1\t|3\t|7\t|9" << std::endl;

	std::ifstream primeFile("primes.txt");
	std::vector<int> primes;
	std::string line;

	int lastPrimeDigit = 0;

	while (std::getline(primeFile, line)) {
		int n = std::stoi(line);
		if (n == 5 || n == 2) {
			continue;
		}

		int lastDigitNumber = lastDigit(n);
		if (lastPrimeDigit == 0) {
			lastPrimeDigit = lastDigitNumber;
			continue;
		}

		countFollowingLastDigit[lastPrimeDigit].count[lastDigitNumber]++;
		countFollowingLastDigit[lastPrimeDigit].total++;
		lastPrimeDigit = lastDigitNumber;
	}

	std::map<int, std::map<int, float>> stat;
	std::cout.precision(4);
	std::for_each(countFollowingLastDigit.begin(), countFollowingLastDigit.end(), [&stat](auto pairDigitToData) {
		std::cout << pairDigitToData.first << "\t";
		std::for_each(pairDigitToData.second.count.begin(), pairDigitToData.second.count.end(), [&stat, pairDigitToData](auto pairDigitToCount) {
			stat[pairDigitToData.first][pairDigitToCount.first] = pairDigitToCount.second / (float)pairDigitToData.second.total;
			std::cout << stat[pairDigitToData.first][pairDigitToCount.first] * 100 << "\t";
			});
		std::cout << std::endl;
		});
	return 0;
}
